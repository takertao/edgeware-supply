## edgeware-supply

A Zeit service for retrieving the total issuance of Edgeware.
Example: https://edgeware-supply.now.sh/

### Installing

- yarn install
- Set up a Zeit/Now account

### Deploying

- git clone this repository
- `now deploy --prod`
